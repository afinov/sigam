Categorie produit
=================

.. toctree::
	:maxdepth: 1
	:titlesonly:

Cette option permet de définir les différentes opérations à utiliser pour passer des écritures sur les comptes des fournisseurs

.. image:: ../_img/referentiels/categorieProduit1.png
	:alt: liste
	:align: center
	:height: 165
	:width: 900
	
   
**Edition de la fiche : Categorie produit**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Toutes les zones de cet écran sont obligatoire.

	* **Code** : Indiquez le code de l’opération. Ce code est unique dans l’application.
	* **Libellé** : Indiquez la désignation de l’opération.
	
.. image:: ../_img/referentiels/categorieProduit2.png
	:alt: édition
	:align: center
	:height: 172
	:width: 800

