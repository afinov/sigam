Plan Comptable
==============

.. toctree::
	:maxdepth: 1
	:titlesonly:

Cette option permet de définir le plan comptable qui sera utilisé dans la comptabilisation.

.. image:: ../_img/referentiels/planComptable1.png
	:alt: liste
	:align: center
	:height: 165
	:width: 900
	
	
**Edition de la fiche : Plan comptable**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

les zones ci-dessous de cet écran sont obligatoires.

	* **Code** : Indiquez le code.
	* **Libellé** : Indiquez le libellé.
	* **Auxilliaire** : Oui, elle indique que ce plan est utlisé dans la comptabilité auxilliaire.
	* **Analytique** : Oui, elle indique que ce plan est utlisé dans la comptabilité analytique.
	
.. image:: ../_img/referentiels/planComptable2.png
	:alt: édition
	:align: center
	:height: 172
	:width: 800
	