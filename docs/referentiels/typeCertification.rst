Type Certification
=====================

.. toctree::
	:maxdepth: 1
	:titlesonly:

Cette option permet de définir les types de certification des produits.

.. image:: ../_img/referentiels/typeCertification1.png
	:alt: liste
	:align: center
	:height: 165
	:width: 900
	
	
**Edition de la fiche : Type Certification**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

les zones ci-dessous de cet écran sont obligatoires.

	* **Code** : Indiquez le code.
	* **Libellé** : Indiquez le libellé.
	
.. image:: ../_img/referentiels/typeCertification2.png
	:alt: édition
	:align: center
	:height: 172
	:width: 800
	