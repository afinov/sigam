Critère d'analyse
=================

.. toctree::
	:maxdepth: 1
	:titlesonly:

Cette option permet de declarer les critères qui seront mesurés lors de l'analyse des échantillons des produits.

.. image:: ../_img/referentiels/critereAnalyse1.png
	:alt: liste
	:align: center
	:height: 165
	:width: 900
	
	
**Edition de la fiche : Critère d'analyse**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

L'écran d'édition de critère d'analyse se divise en quatre parties.

**1ère partie** : Elle vous permet d'indiquer les informations élémentaires sur le critère d'analyse. 

les zones ci-dessous de cet écran sont obligatoires.

	* **Code** : Indiquez le code.
	* **Libellé** : Indiquez le libellé.
	* **Groupe** : Indiquez le groupe (cela permet de faire des regroupements).
	
les autres zones facultatives sont les suivantes.

    * **Libellé 2** : Indiquez le deuxième libellé .
	* **Libellé 3** : Indiquez le troisième libellé.
	* **Sigle** : Indiquez le sigle (cela permet de racourcis les libellés de certains critères d'analyse).
	* **Regroupement quotidien** : indiquez le regroupement quotidien.

.. image:: ../_img/referentiels/critereAnalyse2.png
	:alt: édition
	:align: center
	:height: 172
	:width: 800


**2ème partie - Achat** : Elle vous permet d'indiquer le format de la valeur du critère d'analyse et son mode d'affichage sur les rapports. 

	* **Saisie en pourcentage** : Activée, elle indique que la valeur de ce critère d'analyse sera saisie en pourcentage.
	* **Imprimable** : Activée, elle indique que ce critère d'analyse sera visible sur les rapports.
	* **Afficher les decimals** : Activée, elle indique que la valeur de ce critère d'analyse sera affichée en nombre à virgule sur les rapports.
	* **Refaction en pourcentage** : Activée, elle indique que la refaction qui est issu du calcul des valeurs d'analyse sera evaluée en pourcentage.
	* **Imprimable en %** : Activée, elle indique que ce critère d'analyse sera visible sous la forme de pourcentage sur les rapports.
	* **Afficher le signe pourcentage** : Activée, elle indique que la valeur de ce critère d'analyse sera affichée suivi du signe % sur les rapports.
	
.. image:: ../_img/referentiels/critereAnalyse3.png
	:alt: édition
	:align: center
	:height: 172
	:width: 800
	


**3ème partie - Usinage**: Elle vous permet d'indiquer le format de la valeur du critère d'analyse et son mode d'affichage sur les rapports. 

	* **Saisie en pourcentage** : Activée, elle indique que la valeur de ce critère d'analyse sera saisie en pourcentage.
	* **Imprimable** : Activée, elle indique que ce critère d'analyse sera visible sur les rapports.
	* **Afficher les decimals** : Activée, elle indique que la valeur de ce critère d'analyse sera affichée en nombre à virgule sur les rapports.
	* **Refaction en pourcentage** : Activée, elle indique que la refaction qui est issu du calcul des valeurs d'analyse sera evaluée en pourcentage.
	* **Imprimable en %** : Activée, elle indique que ce critère d'analyse sera visible sous la forme de pourcentage sur les rapports.
	* **Afficher le signe pourcentage** : Activée, elle indique que la valeur de ce critère d'analyse sera affichée suivi du signe % sur les rapports.
	
.. image:: ../_img/referentiels/critereAnalyse4.png
	:alt: édition
	:align: center
	:height: 172
	:width: 800

	
**4ème partie**: Elle vous permet d'indiquer l'affichage des critères sur des rapports specifiques. 

	* **Afficher sur rapport complet** : Oui, elle indique que la valeur de ce critère d'analyse s'affche sur un rapport complet.
	* **Afficher sur rapport réduit** :  Oui, elle indique que la valeur de ce critère d'analyse s'affche sur un rapport reduit.
	* **Surbrillance en HN** : Oui, elle indique que la valeur de ce critère d'analyse sera affichée en fond gras.
	
.. image:: ../_img/referentiels/critereAnalyse5.png
	:alt: édition
	:align: center
	:height: 172
	:width: 800

