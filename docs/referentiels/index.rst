.. SIGAM documentation master file, created by
   sphinx-quickstart on Wed Apr 24 01:56:11 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Référentiels
============

.. image:: ../_img/home/referentiel.jpg

Le référentiel vous donne accès à toutes les fonctionnalités de configuration du logiciel et permettant la bonne exploitation de SIGAM.

Table des matières :

.. toctree:: :maxdepth: 2

 armateur
 banque
 campagne
 codeOperation
 compteBancaire
 compteGeneral
 devise
 documentProduit
 emballage
 familleProduit
 familleTaxe
 groupeAnalyse
 groupeTaxe
 intervenant
 journalComptable
 magasin
 marque
 origine
 paletteLot
 pays
 pontBascule
 port
 produit
 proprietaire
 qualite
 recolte
 region
 site
 tarifConnaissement
 tarifNegocie
 tarifProduit
 tarifTransport
 taxe
 taxeProduit
 tiers
 traitant
 transitaire
 tremie
 typeAnalyse
 typeDocument
 typeSousProduit
 typeTraitant
 usine
 zone



