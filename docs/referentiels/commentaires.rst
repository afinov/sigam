Commentaires
============

.. toctree::
	:maxdepth: 1
	:titlesonly:

Cette option permet de définir les commentaires à afficher sur les differentes catégories de documents au niveau du shipping

.. image:: ../_img/referentiels/commentaire1.png
	:alt: liste
	:align: center
	:height: 165
	:width: 900
	
	
**Edition de la fiche : Commentaires**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Les zones ci-dessous de cet écran sont obligatoires.

	* **Description** : Indiquez le commentaire du document.
	* **Catégorie** : Indiquez la catégorie de document sur lequelle sera affichée le commentaire .
	
Les autres zones facultatives sont les suivantes.

    * **Commentaires** : Indiquez le commentaire .

.. image:: ../_img/referentiels/commentaire2.png
	:alt: édition
	:align: center
	:height: 172
	:width: 800

