Type Compteur
=====================

.. toctree::
	:maxdepth: 1
	:titlesonly:

Cette option permet de définir les types de compteur.

.. image:: ../_img/referentiels/typeCompteur1.png
	:alt: liste
	:align: center
	:height: 165
	:width: 900
	
	
**Edition de la fiche : Type Compteur**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

les zones ci-dessous de cet écran sont obligatoires.

	* **Code** : Indiquez le code.
	* **Libellé** : Indiquez le libellé.
	* **Unique ?** : Oui, elle indique l'unicité du type de compteur.
	* **Par Produit ?** : Oui, elle indique que le type de compteur est généré par produit.
	
.. image:: ../_img/referentiels/typeCompteur2.png
	:alt: édition
	:align: center
	:height: 172
	:width: 800
	