Grade
=====

.. toctree::
	:maxdepth: 1
	:titlesonly:

Cette option permet de definir les grades qui seront determinées lors de la determination des qualités des produits.

.. image:: ../_img/referentiels/grade1.png
	:alt: liste
	:align: center
	:height: 165
	:width: 900
	
	
**Edition de la fiche : Grade**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

L'écran d'édition de grade se divise en deux parties.

**1ère partie** : Elle vous permet d'indiquer les informations concernant la categorie de grade. 

les zones ci-dessous de cet écran sont obligatoires.

	* **Code** : Indiquez le code.
	* **Libellé** : Indiquez le libellé.
	
.. image:: ../_img/referentiels/grade2.png
	:alt: édition
	:align: center
	:height: 172
	:width: 800
	

**2ème partie** : Elle vous permet d'indiquer les grades qui appartiennent à la categorie ci-dessus mentionée. 

	* **Code** : Indiquez le code.
	* **Libellé** : Indiquez le libellé.
	* **Ajouter** : Cliquez sur le bouton **Ajouter** pour valider la ligne saisie.
	* **Supprimer** : Cliquez sur le bouton **Supprimer** pour retirer la ligne erronée ou non souhaitée .
	
.. image:: ../_img/referentiels/grade3.png
	:alt: édition
	:align: center
	:height: 172
	:width: 800
	
	A la fin de votre saisie, afin de valider vos modifications, n'oubliez pas de cliquer sur le bouton **Enregistrer**
	
