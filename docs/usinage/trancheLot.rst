Tranche de lot
==============

.. toctree::
	:maxdepth: 1
	:titlesonly:

Cette fonctionalité vous permet de créer les tranches de lot dans lesquelles les lots sont fabriqués.

.. image:: ../_img/usniages/trancheLot/listeTrancheLot.png
	:alt: liste
	:align: center
	:height: 237
	:width: 800
	
   
**Edition de la fiche : Tranche de lot**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

les zones ci-dessous de cet écran sont obligatoires.

	* **Type mouvement** : indiquez le type de mouvement.
	* **Date** : indiquez la date de saisie.
	* **traitant/Fournisseur** : indiquez le fournisseur du produit.
	* **Produit** : indiquez le produit.
	* **Exportateur/Propriétaire** : indiquez le propriétaire du produit.
	* **Marque** : indiquez la marque du produit.
	* **Origine** : indiquez l'origine du produit.
	* **Destination** : indiquez la destination du produit.
	* **Type de sacs** : indiquez le type de sac.
	* **Etat des sacs** : indiquez l'état des sacs.
	* **Nb.Sacs** : indiquez le nombre de sacs.
	* **Pèse palette** : indiquez la pèse palette ou la balance.
	* **Poids** : indiquez le poids du produit.

.. image:: ../_img/usniages/trancheLot/editTrancheLot.png
	:alt: édition
	:align: center
	:height: 257
	:width: 600

