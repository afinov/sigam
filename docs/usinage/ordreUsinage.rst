Ordre d'usinage
===============

.. toctree::
	:maxdepth: 1
	:titlesonly:

Cette fonctionalité vous permet d'identifier chaque opération de fabrication de lot par un code(indice) qui servira dans le calcul de la freinte d'usinage.

.. image:: ../_img/usniages/ordreUsinage/listeOrdreUsinage.png
	:alt: liste
	:align: center
	:height: 237
	:width: 800
	
   
**Edition de la fiche : Ordre d'usinage**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

L'écran d'édition de l'ordre d'usinage se divise en 3 parties. Ces trois parties sont neccessaires dans le cas d'utilisation d'un programme d'usinage.

**1ère partie** : Elle vous permet d'indiquer les informations élémentaires sur l'ordre d'usinage 

Certaines zones ci-dessous de cet écran sont obligatoires.

	* **N° Ordre** : indiquez le numéro d'ordre d'usinage (Ce numéro doit ètre unique pendant une campagne).
	* **Date** : indiquez la date de saisie.
	* **Propriétaire** : indiquez le propriétaire du produit.
	* **Produit** : indiquez le produit.
	* **Qualité** : indiquez la qualité du produit.
	* **Cloture** : Oui, elle indique que cet ordre d'usinage est cloturé.

.. image:: ../_img/usniages/ordreUsinage/editOrdreUsinage1.png
	:alt: édition
	:align: center
	:height: 257
	:width: 600


**2ème partie** : Elle vous permet d'associer des tranches de lot à l'ordre d'usinage.

	* **Ajouter une tranche lot** : Cliquez sur ce le bouton **Ajouter une tranche lot** pour choisir une tranche de lot à associer.

.. image:: ../_img/usniages/editOrdreUsinage2.png
	:alt: édition
	:align: center
	:height: 257
	:width: 600
	
	
**3ème partie** : Elle vous permet d'associer des types de stock(stoks brousse,lot,balayures etc.) à l'ordre d'usinage.

	* **Numéro Ordre** : indiquez la numerotation
	* **Connaissement** : Cliquez sur ce le bouton **Connaissement** pour choisir un stock brousse à associer.
	* **Lot** : Cliquez sur ce le bouton **Lot** pour choisir un lot à associer.
	* **Balayure** : Cliquez sur ce le bouton **Balayure** pour choisir un stock de balayure à associer.
	* **Echantillon** : Cliquez sur ce le bouton **Echantillon** pour choisir un stock d'échantillon à associer.
	* **Sous produits** : Cliquez sur ce le bouton **Sous produits** pour choisir un stock de sous produits à associer.
	* **Report de stock** : Cliquez sur ce le bouton **Report de stock** pour choisir un report de stock à associer.

.. image:: ../_img/usniages/ordreUsinage/editOrdreUsinage3.png
	:alt: édition
	:align: center
	:height: 257
	:width: 600
	
	