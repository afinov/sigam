Mise en fosse
=============

.. toctree::
	:maxdepth: 1
	:titlesonly:

Cette fonctionalité vous permet de gerer les déversements des stocks de produit brousse dans les tremies(silos).

.. image:: ../_img/usniages/miseEnfosse/listeMiseEnfosse.png
	:alt: liste
	:align: center
	:height: 237
	:width: 800
	
   
**Edition de la fiche : Mise en fosse**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

les zones ci-dessous de cet écran sont obligatoires.

	* **Date versement** : indiquez la date de l'opération.
	* **ordre usinage** : indiquez l'ordre d'usinage.
	* **Trémie** : indiquez la trémie.
	* **Pont bascule** : indiquez le pont bascule ou pèse palette.
	* **Tare sacs** : indiquez la tare des sacs.
	* **Tare palette** : indiquez la tare des palettes.
	* **Type de versement** : choisir le type de versement.

.. image:: ../_img/usniages/miseEnfosse/editMiseEnfosse.png
	:alt: édition
	:align: center
	:height: 257
	:width: 600

