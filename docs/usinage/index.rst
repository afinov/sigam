Module Usinage
==============

Le module d'usinage vous donne accès aux fonctionnalités de fabrication et de magasinage des produits usinés (lots).

Les différentes opérations suivent un workflow prédéfinis qui est le suivant :

.. image:: ../_img/home/usniages.png
	:height: 400

Table des matières :

.. toctree:: :maxdepth: 2

 trancheLot
 ordreUsinage
 miseEnfosse




