Information de sortie
=====================

.. toctree::
	:maxdepth: 1
	:titlesonly:

Cette fonctionalité vous permet de consulter les informations sur le connaissement depuis l'entrée du camion au pont bascule jusqu'a sa sortie.

Sur de chaque document (ligne) les boutons à droite, vous permettent de **modifier**, ou **imprimer** le document.

.. image:: ../_img/achats/infoSortie/listeInfoSortie.png
	:alt: liste
	:align: center
	:height: 188
	:width: 798

**Visualisation de la fiche : Information de sortie**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Pour visualiser un document (ligne), vous devez cliquez sur l'icone de **recherche**.

Pour modifier un document (ligne), vous devez cliquez sur **modifier**.

.. image:: ../_img/achats/infoSortie/infoSortie.png
	:alt: édition
	:align: center
	:height: 257
	:width: 600
	