Balayures
=========

.. toctree::
	:maxdepth: 1
	:titlesonly:

Cette fonctionalité vous permet de gerer le stock des balayures brousses.

.. image:: ../_img/achats/balayures/listeBalayure.png
	:alt: liste
	:align: center
	:height: 237
	:width: 800
	
   
**Edition de la fiche : Balayures**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

les zones ci-dessous de cet écran sont obligatoires.

	* **Date** : indiquez la date de saisie.
	* **Produit** : indiquez le produit.
	* **Pont bascule** : indiquez le pont bascule.
	* **Poids** : indiquez le poids du produit.
	* **Nb sac** : indiquez le nombre de sacs.
	* **Tare Sac** : indiquez la tare de sac.
	* **Tare Palette** : indiquez la tare de la palette.
	
.. image:: ../_img/achats/balayures/editionBalayure.png
	:alt: édition
	:align: center
	:height: 257
	:width: 600

