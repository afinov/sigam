Repartion
=========

.. toctree::
	:maxdepth: 1
	:titlesonly:

Cette fonctionalité vous permet de faire la repartition d'un connaissement en plusieurs.

les zones ci-dessous de cet écran sont obligatoires.

	* **N CNT** : indiquez le numéro du connaissement.

.. image:: ../_img/achats/repartition/editRepartion.png
	:alt: liste
	:align: center
	:height: 257
	:width: 600
	

