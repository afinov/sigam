Etat Stock
==========

.. toctree::
	:maxdepth: 1
	:titlesonly:

Cette fonctionalité vous permet de visualiser l'état du stock de la sacherie.

les zones ci-dessous de cet écran sont obligatoires.

	* **Date** : indiquez le numéro du connaissement.
	* **Type sac ** : indiquez le type de sac.

.. image:: ../_img/achats/etatStock/editEtatStock.png
	:alt: liste
	:align: center
	:height: 257
	:width: 600
	

