1ère Pesée
==========

.. toctree::
	:maxdepth: 1
	:titlesonly:

Cette fonctionalité vous permet de capter les poids des camions chargés de produits qui arrivent au pont bascule.

.. image:: ../_img/achats/1erePesee/liste1erePesee.png
	:alt: liste
	:align: center
	:height: 237
	:width: 800
	
   
**Edition de la fiche : 1ère Pesée**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Pour avoir le poids brut du camion chargé, vous devez cliquez sur le bouton « **Auto** » ou saisir le poids brut (en fonction de votre profil)

Toutes les zones de cet écran sont obligatoire.

	* **Pont bascule** : indiquez le pont bascule.
	* **Poids brut** : indiquez le poids brut.

.. image:: ../_img/achats/1erePesee/1erePesee.png
	:alt: édition
	:align: center
	:height: 257
	:width: 600

