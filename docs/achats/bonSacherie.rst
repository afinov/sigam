Bon sacherie
============

.. toctree::
	:maxdepth: 1
	:titlesonly:

Cette fonctionalité vous permet de gerer le stock de la sacherie.

.. image:: ../_img/achats/bonSacherie/listeBonSacherie.png
	:alt: liste
	:align: center
	:height: 237
	:width: 800
	
   
**Edition de la fiche : Bon sacherie**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

les zones ci-dessous de cet écran sont obligatoires.

	* **Type de mouvement** : indiquez le type de mouvement.
	* **Numéro Bon** : indiquez le numéro de bon.
	* **Date** : indiquez la date de saisie.
	* **Produit** : indiquez le produit.
	* **Propriétaire** : indiquez le propriétaire.
	* **Fournisseur** : indiquez le fournisseur.
	* **Type de sac** : indiquez le type de sac.
	* **Etat de sac** : indiquez l'état de sac.
	* **Quantité** : indiquez le nombre de sacs.
	
.. image:: ../_img/achats/bonSacherie/editionBonSacherie.png
	:alt: édition
	:align: center
	:height: 257
	:width: 600

