2ème Pesée
==========

.. toctree::
	:maxdepth: 1
	:titlesonly:

Cette fonctionalité vous permet de capter les poids des camions à vide qui arrivent au pont bascule.

.. image:: ../_img/achats/2emePesee/liste2emePesee.png
	:alt: liste
	:align: center
	:height: 237
	:width: 800
	
   
**Edition de la fiche : 2ème Pesée**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Pour avoir le poids brut du camion à vide, vous devez cliquez sur le bouton « **Auto** » ou saisir le poids brut (en fonction de votre profil)

les zones ci-dessous de cet écran sont obligatoires.

	* **Pont bascule** : indiquez le pont bascule.
	* **Poids brut** : indiquez le poids brut.
	* **Nb sacs déchargés/chargés** : indiquez le nombre de sacs dechargés ou chargés.
	* **Chauffeur** : indiquez le nom du chauffeur.
	* **Immat camion** : indiquez l'immatriculation du camion.
	* **Immat remorque** : indiquez l'immatriculation de la remorque.
	* **Details sacs** : indiquez les détails des sacs.

.. image:: ../_img/achats/2emePesee/2emePesee.png
	:alt: édition
	:align: center
	:height: 257
	:width: 600

