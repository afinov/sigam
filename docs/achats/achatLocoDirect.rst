Achat loco (Direct)
====================

.. toctree::
	:maxdepth: 1
	:titlesonly:

Cette fonctionalité vous permet de gerer les cessions des lots usinés qui proviennent d'un fournisseur.

Sur de chaque document (ligne) les boutons à droite, vous permettent de **modifier**, ou **generer** les lots qui y figurent.

.. image:: ../_img/achats/achatLoco/listeAchatLoco.png
	:alt: liste
	:align: center
	:height: 237
	:width: 800
	
   
**Edition de la fiche : Achat loco (Direct)**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

les zones ci-dessous de cet écran sont obligatoires.

	* **Date** : indiquez la date de saisie.
	* **Contrats achats locaux** : indiquez le contrat de cession.
	* **Prix d'achat** : indiquez le prix d'achat du produit.
	* **Origine * : indiquez l'origine du produit.
	* **Fournisseur** : indiquez le fournisseur.
	* **Produit** : indiquez le produit.
	* **Marque** : indiquez la marque.
	* **Propriétaire** : indiquez le propriétaire.
	* **Numero de lot** : indiquez le numéro de lot.
	* **Poids Net** : indiquez le poids net du lot.
	* **Nbre sac** : indiquez le nombre de sacs.
	* **Tare sac** : indiquez la tare des sacs.
	* **Tare palette** : indiquez la tare de la palette.
	* **Date magasinage** : indiquez la date d'entrée en magasin.
	* **Type sacs** : indiquez le type de sac.
	* **Etat sac** : indiquez l'état des sacs .
	* **Qualité** : indiquez la qualité des produits.
	
.. image:: ../_img/achats/achatLoco/editAchatLoco.png
	:alt: édition
	:align: center
	:height: 257
	:width: 600

