Contrat loco magasin
====================

.. toctree::
	:maxdepth: 1
	:titlesonly:

Cette fonctionalité vous permet de gerer les contrats de cesion des lots usinés.

.. image:: ../_img/achats/contratLocoMagasin/listeContratLocoMagasin.png
	:alt: liste
	:align: center
	:height: 237
	:width: 800
	
   
**Edition de la fiche : Contrat loco magasin**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

les zones ci-dessous de cet écran sont obligatoires.

	* **Date** : indiquez la date de saisie.
	* **Fournisseur** : indiquez le fournisseur.
	* **Produit** : indiquez le produit.
	* **Marque** : indiquez la marque.
	* **Nb. Sacs** : indiquez le nombre de sacs.
	* **Prix d'achat** : indiquez le prix d'achat du produit.
	* **Poids** : indiquez le poids.
	
.. image:: ../_img/achats/contratLocoMagasin/editionContratLocoMagasin.png
	:alt: édition
	:align: center
	:height: 257
	:width: 600

