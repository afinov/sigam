Correction Achats
=================

.. toctree::
	:maxdepth: 1
	:titlesonly:

Cette fonctionalité vous permet de modifier les informations élémentaires (sans les poids) sur les connaissements qui ont fait l'objet de deuxième pesée.

.. image:: ../_img/achats/correctionAchat/listeCorrectionAchat.png
	:alt: liste
	:align: center
	:height: 188
	:width: 798

**Edition de la fiche : Correction Achats**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Les zones de saisies grisées ne sont pas modifiables.

.. image:: ../_img/achats/correctionAchat/editCorrectionAchat.png
	:alt: édition
	:align: center
	:height: 257
	:width: 600
	