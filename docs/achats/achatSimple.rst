Achat simple
============

.. toctree::
	:maxdepth: 1
	:titlesonly:

Cette fonctionalité vous permet de gerer les stocks(entrées et sorties) des produits bords-champs.

.. image:: ../_img/achats/achatSimple/listeAchatSimple.png
	:alt: liste
	:align: center
	:height: 237
	:width: 800
	
   
**Edition de la fiche : Achat simple**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

les zones ci-dessous de cet écran sont obligatoires.

	* **Type mouvement** : indiquez le type de mouvement.
	* **Date** : indiquez la date de saisie.
	* **traitant/Fournisseur** : indiquez le fournisseur du produit.
	* **Produit** : indiquez le produit.
	* **Exportateur/Propriétaire** : indiquez le propriétaire du produit.
	* **Marque** : indiquez la marque du produit.
	* **Origine** : indiquez l'origine du produit.
	* **Destination** : indiquez la destination du produit.
	* **Type de sacs** : indiquez le type de sac.
	* **Etat des sacs** : indiquez l'état des sacs.
	* **Nb.Sacs** : indiquez le nombre de sacs.
	* **Pèse palette** : indiquez la pèse palette ou la balance.
	* **Poids** : indiquez le poids du produit.

.. image:: ../_img/achats/achatSimple/editionAchatSimple.png
	:alt: édition
	:align: center
	:height: 257
	:width: 600

