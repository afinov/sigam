Divers sortie
=============

.. toctree::
	:maxdepth: 1
	:titlesonly:

Cette fonctionalité vous permet de gérer les sorties de produit sans toute fois impacter obligatoirement le stock de l'usine.
  
**Edition de la fiche : Divers sortie**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

les zones ci-dessous de cet écran sont obligatoires.

    * **Type campagne** : indiquez le type de campagne.
	* **N° CNT** : indiquez le numéro du connaissement.
	* **Type produit à sortir** : indiquez le type de produit à faire sortir de l'usine.
	* **Date** : indiquez la date de saisie.
	* **Produit** : indiquez le produit.
	* **Marque** : indiquez la marque du produit.
	* **Propriétaire** : indiquez le propriétaire du produit.
	* **Fournisseur** : indiquez le fournisseur du produit.
	* **Origine** : indiquez l'origine du produit.
	* **Destination** : indiquez la destination du produit.
	* **Chauffeur** : indiquez le nom du chauffeur.
	* **Immat camion** : indiquez l'immatriculation du camion.
	* **Immat remorque** : indiquez l'immatriculation de la remorque.

.. image:: ../_img/achats/infoEntree/diversSortie.png
	:alt: édition
	:align: center
	:height: 257
	:width: 600

