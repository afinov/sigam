Transfert sortie
================

.. toctree::
	:maxdepth: 1
	:titlesonly:

Cette fonctionalité vous permet de gérer les transfert de produits brousses d'une usine à une autre.
  
**Edition de la fiche : Transfert sortie**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

les zones ci-dessous de cet écran sont obligatoires.

    * **Type campagne** : indiquez le type de campagne.
	* **N° CNT** : indiquez le numéro du connaissement.
	* **Site de déchargement** : indiquez le site de déchargement.
	* **Usine de destination** : indiquez l'usine de destination.
	* **Date** : indiquez la date de saisie.
	* **Produit** : indiquez le produit.
	* **Marque** : indiquez la marque du produit.
	* **Propriétaire** : indiquez le propriétaire du produit.
	* **Origine** : indiquez l'origine du produit.
	* **Destination** : indiquez la destination du produit.
	* **Chauffeur** : indiquez le nom du chauffeur.
	* **Immat camion** : indiquez l'immatriculation du camion.
	* **Immat remorque** : indiquez l'immatriculation de la remorque.

.. image:: ../_img/achats/infoEntree/transfertSortie.png
	:alt: édition
	:align: center
	:height: 257
	:width: 600

