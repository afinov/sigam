Transfert entrée
================

.. toctree::
	:maxdepth: 1
	:titlesonly:

Cette fonctionalité vous permet de gérer la reception des produits brousses provenant d'une usine tierce.
  
**Edition de la fiche : Transfert entrée**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

les zones ci-dessous de cet écran sont obligatoires.

    * **Type campagne** : indiquez le type de campagne.
	* **N° CNT** : indiquez le numéro du connaissement.
	* **Site de déchargement** : indiquez le site de déchargement.
	* **CNT Transfert** : indiquez le connaisement à transferer.
	* **Chauffeur** : indiquez le nom du chauffeur.
	* **Immat camion** : indiquez l'immatriculation du camion.
	* **Immat remorque** : indiquez l'immatriculation de la remorque.

.. image:: ../_img/achats/infoEntree/transfertEntree.png
	:alt: édition
	:align: center
	:height: 257
	:width: 600

