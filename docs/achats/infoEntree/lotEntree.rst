Lot entrée
==========

.. toctree::
	:maxdepth: 1
	:titlesonly:

Cette fonctionalité vous permet de gérer les entrées des lots (produits usinés) par la pesée directe sur le pont bascule de l'unsine receptrice.
  
**Edition de la fiche : Lot entrée**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

les zones ci-dessous de cet écran sont obligatoires.

    * **Type campagne** : indiquez le type de campagne.
	* **N° CNT** : indiquez le numéro du connaissement.
	* **Site de déchargement** : indiquez le site de déchargement.
	* **Date** : indiquez la date de saisie.
	* **Produit** : indiquez le produit.
	* **Marque** : indiquez la marque du produit.
	* **Propriétaire** : indiquez le propriétaire du produit.
	* **Fournisseur** : indiquez le fournisseur du produit.
	* **Origine** : indiquez l'origine du produit.
	* **Destination** : indiquez la destination du produit.
	* **Chauffeur** : indiquez le nom du chauffeur.
	* **Immat camion** : indiquez l'immatriculation du camion.
	* **Immat remorque** : indiquez l'immatriculation de la remorque.

.. image:: ../_img/achats/infoEntree/lotEntree.png
	:alt: édition
	:align: center
	:height: 257
	:width: 600

