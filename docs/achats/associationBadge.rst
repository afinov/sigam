Association de Badge
====================

.. toctree::
	:maxdepth: 1
	:titlesonly:

Cette fonctionalité vous permet de gerer les codifications des échantillons des produits à analyser.

.. image:: ../_img/achats/associationBadge/listeBadges.png
	:alt: liste
	:align: center
	:height: 237
	:width: 800
	
   
**Edition de la fiche : Association de Bage**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Pour faire une analyse de masse, vous devez cliquer sur le bouton « **Analyse de masse:Non** » pour l'activer en « **Analyse de masse:Oui** ».

les zones ci-dessous de cet écran sont obligatoires.

	* **N° Connaissement** : indiquez le numéro de connaissement.

.. image:: ../_img/achats/associationBadge/associationBage.png
	:alt: édition
	:align: center
	:height: 257
	:width: 600

