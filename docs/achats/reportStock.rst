Report Stock
============

.. toctree::
	:maxdepth: 1
	:titlesonly:

Cette fonctionalité vous permet de gerer les reports de stocks d'une campagne.

.. image:: ../_img/achats/reportStock/validationAnalyse/listeReportStock.png
	:alt: liste
	:align: center
	:height: 237
	:width: 800
	
   
**Edition de la fiche : Report Stock**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

les zones ci-dessous de cet écran sont obligatoires.

    * **Code** : indiquez le code de l'operation.
	* **Date** : indiquez la date de saisie.
	* **Propriétaire** : indiquez le propriétaire.
	* **Produit** : indiquez le produit.
	* **Campagne d'origine * : indiquez la campagne d'origine.
	* **Usine source** : indiquez l'usine source.
	* **Usine de destination** : indiquez l'usine de destination.
	* **Poids brut** : indiquez le poids brut.
	* **Poids Net** : indiquez le poids net.
	
.. image:: ../_img/achats/reportStock/validationAnalyse/editReportStock.png
	:alt: édition
	:align: center
	:height: 257
	:width: 600

