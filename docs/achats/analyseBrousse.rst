Saisie d'analyse
=================

.. toctree::
	:maxdepth: 1
	:titlesonly:

Cette fonctionalité vous permet de faire la saisie des analyses effectuées sur des échantillons des produits brousses.

Les gadgets avec l'icone de couleur verte montrent que les analyses des échantillons sont deja enregistrées.

.. image:: ../_img/achats/analyseBrousse/listeAnalyse.png
	:alt: liste
	:align: center
	:height: 237
	:width: 800
	
   
**Edition de la fiche : Saisie d'analyse**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Toutes les zones actives de cet écran sont obligatoires.

.. image:: ../_img/achats/analyseBrousse/saisieAnalyse.png
	:alt: édition
	:align: center
	:height: 257
	:width: 600

