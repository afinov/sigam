Prévision
====================

.. toctree::
	:maxdepth: 1
	:titlesonly:

Cette fonctionalité vous permet de planifier des prévisions de stock en achat, en usinage et en shipping.

.. image:: ../_img/achats/prevision/listePrevision.png
	:alt: liste
	:align: center
	:height: 237
	:width: 800
	
   
**Edition de la fiche : Prévision**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

les zones ci-dessous de cet écran sont obligatoires.

	* **Usine** : indiquez l'usine de stockage.
	* **Produit** : indiquez le produit.
	* **Type** : indiquez le type de stock.
	* **Date** : indiquez la date de saisie.
	* **Poids** : indiquez le poids.

.. image:: ../_img/achats/prevision/editPrevision.png
	:alt: édition
	:align: center
	:height: 257
	:width: 600

