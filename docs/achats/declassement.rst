Declassement
============

.. toctree::
	:maxdepth: 1
	:titlesonly:

Cette fonctionalité vous permet de remplacer uniquement le produit d'un connaissement par un autre de meme famille que le prémier.

Vous pouvez utiliser cette fonctionalité si le connaissement à fait sa deuxième pesée.

les zones ci-dessous de cet écran sont obligatoires.

	* **N° CNT** : indiquez le numéro du connaissement.
	* **Choisir un produit** : indiquez le produit correct.

.. image:: ../_img/achats/declassement/editDeclassement.png
	:alt: liste
	:align: center
	:height: 257
	:width: 600
	

