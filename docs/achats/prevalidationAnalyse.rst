Prévalidation d'analyse
=======================

.. toctree::
	:maxdepth: 1
	:titlesonly:

Cette fonctionalité vous permet de faire la validation des analyses de masse effectuées sur des échantillons des produits brousses.

.. image:: ../_img/achats/prevalidationAnalyse/listePreanalyse.png
	:alt: liste
	:align: center
	:height: 237
	:width: 800
	
   
**Edition de la fiche : Prévalidation d'analyse**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cliquez sur le bouton « **Pre-Validation** » de cet écran pour valider les analyses de masse.

.. image:: ../_img/achats/prevalidationAnalyse/prevalidationAnalyse.png
	:alt: édition
	:align: center
	:height: 257
	:width: 600

